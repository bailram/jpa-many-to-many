package com.bailram.jpamanytomany;

import com.bailram.jpamanytomany.domain.entity.Book;
import com.bailram.jpamanytomany.domain.entity.Tag;
import com.bailram.jpamanytomany.repository.BookRepository;
import com.bailram.jpamanytomany.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaManyToManyApplication implements CommandLineRunner {
	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private TagRepository tagRepository;

	public static void main(String[] args) {
		SpringApplication.run(JpaManyToManyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// Cleanup the tables
		bookRepository.deleteAllInBatch();
		tagRepository.deleteAllInBatch();

		// Create a book
		Book book = new Book("Learn Hibernate Many to Many with spring boot", 120000);

		// Create two tags
		Tag tag1 = new Tag("Spring Boot");
		Tag tag2 = new Tag("Hibernate");

		// Add tag references in the book
		book.getTags().add(tag1);
		book.getTags().add(tag2);

		// Add book refenrences in the tags
		tag1.getBooks().add(book);
		tag2.getBooks().add(book);

		// Save book references (which will save the tag as well)
		bookRepository.save(book);
	}
}
